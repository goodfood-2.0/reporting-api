﻿using DAL.Entity;

namespace BLL.Interface
{
    public interface IReportingOrderServices
    {
        public Task<CountOrders> CountOrders(int idRestaurant, DateTime dateMin, DateTime dateMax);
        public Task<List<TopBestContent>> GetOrderBestContents(int NumlberOfBest, int idRestaurant, DateTime dateMin, DateTime dateMax);
        public Task<List<TopSoldContent>> GetOrderBestSold(int NumlberOfBest, int idRestaurant, DateTime dateMin, DateTime dateMax);
        public Task<TotalAmount> GetTotalAmount(int idRestaurant, DateTime dateMin, DateTime dateMax);
    }
}
