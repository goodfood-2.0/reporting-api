﻿using BLL.Interface;
using DAL.Entity;
using DAL.Interface;

namespace BLL.Service
{
    public class ReportingOrderServices : IReportingOrderServices
    {
        private IReportingOrderOperation _reportingOrderOperation;
        public ReportingOrderServices(IReportingOrderOperation reportingOrderOperation)
        {
            _reportingOrderOperation = reportingOrderOperation;
        }

        public async Task<CountOrders> CountOrders(int idRestaurant, DateTime dateMin, DateTime dateMax)
        {
            return await _reportingOrderOperation.CountOrders(idRestaurant, dateMin, dateMax);
        }

        public async Task<List<TopBestContent>> GetOrderBestContents(int NumlberOfBest, int idRestaurant, DateTime dateMin, DateTime dateMax)
        {
            return await _reportingOrderOperation.GetOrderBestContents(NumlberOfBest, idRestaurant, dateMin, dateMax);
        }

        public async Task<List<TopSoldContent>> GetOrderBestSold(int NumlberOfBest, int idRestaurant, DateTime dateMin, DateTime dateMax)
        {
            return await _reportingOrderOperation.GetOrderBestSold(NumlberOfBest, idRestaurant, dateMin, dateMax);
        }

        public async Task<TotalAmount> GetTotalAmount(int idRestaurant, DateTime dateMin, DateTime dateMax)
        {
            return await _reportingOrderOperation.GetTotalAmount(idRestaurant, dateMin, dateMax);
        }




    }
}
