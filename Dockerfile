#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["reporting-api/reporting-api.csproj", "reporting-api/"]
COPY ["DAL/DAL.csproj", "DAL/"]
COPY ["BLL/BLL.csproj", "BLL/"]
RUN dotnet restore "DAL/DAL.csproj"
RUN dotnet restore "BLL/BLL.csproj"
RUN dotnet restore "reporting-api/reporting-api.csproj"
COPY . .
WORKDIR "/src/reporting-api"
RUN dotnet build "reporting-api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "reporting-api.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "reporting-api.dll"]
