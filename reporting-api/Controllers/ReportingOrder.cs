﻿using BLL.Interface;
using DAL.Entity;
using Microsoft.AspNetCore.Mvc;

namespace reporting_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReportingOrder : ControllerBase
    {

        private readonly ILogger<ReportingOrder> _logger;

        private IReportingOrderServices _reportingOrderServices;

        public ReportingOrder(ILogger<ReportingOrder> logger, IReportingOrderServices reportingOrderServices)
        {
            _logger = logger;
            _reportingOrderServices = reportingOrderServices;
        }

        [HttpGet]
        [Route("GetOrderBestSold")]
        public async Task<ActionResult<List<TopSoldContent>>> GetOrderBestSold(int NumlberOfBest, int idRestaurant, DateTime dateMin, DateTime dateMax)
        {
            List<TopSoldContent> topsold = await _reportingOrderServices.GetOrderBestSold(NumlberOfBest, idRestaurant, dateMin, dateMax);
            if (topsold == null)
            {
                return NotFound();
            }

            return Ok(topsold);
        }

        [HttpGet("GetOrderBestContents")]
        public async Task<ActionResult<List<TopBestContent>>> GetOrderBestContents(int NumlberOfBest, int idRestaurant, DateTime dateMin, DateTime dateMax)
        {
            var topsold = await _reportingOrderServices.GetOrderBestContents(NumlberOfBest, idRestaurant, dateMin, dateMax);
            if (topsold == null)
            {
                return NotFound();
            }
            return Ok(topsold);
        }

        [HttpGet("CountOrders")]
        public async Task<ActionResult<CountOrders>> CountOrders(int idRestaurant, DateTime dateMin, DateTime dateMax)
        {
            var topsold = await _reportingOrderServices.CountOrders(idRestaurant, dateMin, dateMax);
            if (topsold == null)
            {
                return NotFound();
            }
            return Ok(topsold);
        }

        [HttpGet("CountOrders/Week")]
        public async Task<ActionResult<List<CountOrders>>> GetCountOrdersWeek(int idRestaurant)
        {
            List<CountOrders> res = new List<CountOrders>();
            DateTime date = DateTime.Today.AddDays(1).AddSeconds(-1);
            //rajouter la logique ici pour faire 1 appel pour les 7 derniers jours 

            for (int i = 0; i < 7; i++)
            {
                DateTime dateMax = date.AddDays(-1 * i);
                DateTime dateMin = dateMax.AddDays(-1).AddSeconds(1);
                res.Add(await _reportingOrderServices.CountOrders(idRestaurant, dateMin, dateMax));
            }

            if (res.Count == 0)
            {
                return NotFound();
            }
            return Ok(res);
        }

        [HttpGet("CountOrders/Month")]
        public async Task<ActionResult<List<CountOrders>>> GetCountOrdersMonth(int idRestaurant)
        {
            List<CountOrders> res = new List<CountOrders>();
            DateTime date = DateTime.Today.AddDays(1).AddSeconds(-1);
            //rajouter la logique ici pour faire 1 appel pour les 7 derniers jours 

            for (int i = 0; i <= date.Day; i++)
            {
                DateTime dateMax = date.AddDays(-1 * i);
                DateTime dateMin = dateMax.AddDays(-1).AddSeconds(1);
                res.Add(await _reportingOrderServices.CountOrders(idRestaurant, dateMin, dateMax));
            }

            if (res.Count == 0)
            {
                return NotFound();
            }
            return Ok(res);
        }

        [HttpGet("CountOrders/Year")]
        public async Task<ActionResult<List<CountOrders>>> GetCountOrdersYear(int idRestaurant)
        {
            List<CountOrders> res = new List<CountOrders>();
            DateTime date = DateTime.Today.AddDays(-DateTime.Today.Day + 1).AddMonths(1);
            //rajouter la logique ici pour faire 1 appel pour les 7 derniers jours 

            for (int i = 0; i <= 14; i++)
            {
                DateTime dateMax = date.AddMonths(-1 * i);
                DateTime dateMin = dateMax.AddMonths(-1).AddSeconds(1);
                res.Add(await _reportingOrderServices.CountOrders(idRestaurant, dateMin, dateMax));
            }

            if (res.Count == 0)
            {
                return NotFound();
            }
            return Ok(res);
        }

        [HttpGet("GetTotalAmount")]
        public async Task<ActionResult<TotalAmount>> GetTotalAmount(int idRestaurant, DateTime dateMin, DateTime dateMax)
        {
            var topsold = await _reportingOrderServices.GetTotalAmount(idRestaurant, dateMin, dateMax);

            if (topsold == null)
            {
                return NotFound();
            }
            return Ok(topsold);
        }

        [HttpGet("GetTotalAmount/Week")]
        public async Task<ActionResult<List<TotalAmount>>> GetTotalAmountWeek(int idRestaurant)
        {
            List<TotalAmount> res = new List<TotalAmount>();
            DateTime date = DateTime.Today.AddDays(1).AddSeconds(-1);
            //rajouter la logique ici pour faire 1 appel pour les 7 derniers jours 

            for (int i = 0; i < 7; i++)
            {
                DateTime dateMax = date.AddDays(-1 * i);
                DateTime dateMin = dateMax.AddDays(-1).AddSeconds(1);
                res.Add(await _reportingOrderServices.GetTotalAmount(idRestaurant, dateMin, dateMax));
            }

            if (res.Count == 0)
            {
                return NotFound();
            }
            return Ok(res);
        }

        [HttpGet("GetTotalAmount/Month")]
        public async Task<ActionResult<List<TotalAmount>>> GetTotalAmountMonth(int idRestaurant)
        {
            List<TotalAmount> res = new List<TotalAmount>();
            DateTime date = DateTime.Today.AddDays(1).AddSeconds(-1);
            //rajouter la logique ici pour faire 1 appel pour les 7 derniers jours 

            for (int i = 0; i <= date.Day; i++)
            {
                DateTime dateMax = date.AddDays(-1 * i);
                DateTime dateMin = dateMax.AddDays(-1).AddSeconds(1);
                res.Add(await _reportingOrderServices.GetTotalAmount(idRestaurant, dateMin, dateMax));
            }

            if (res.Count == 0)
            {
                return NotFound();
            }
            return Ok(res);
        }

        [HttpGet("GetTotalAmount/Year")]
        public async Task<ActionResult<List<TotalAmount>>> GetTotalAmountYear(int idRestaurant)
        {
            List<TotalAmount> res = new List<TotalAmount>();
            DateTime date = DateTime.Today.AddDays(-DateTime.Today.Day + 1).AddMonths(1);
            //rajouter la logique ici pour faire 1 appel pour les 7 derniers jours 

            for (int i = 0; i <= 14; i++)
            {
                DateTime dateMax = date.AddMonths(-1 * i);
                DateTime dateMin = dateMax.AddMonths(-1).AddSeconds(1);
                res.Add(await _reportingOrderServices.GetTotalAmount(idRestaurant, dateMin, dateMax));
            }

            if (res.Count == 0)
            {
                return NotFound();
            }
            return Ok(res);
        }
    }
}
