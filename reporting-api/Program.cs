using BLL.Interface;
using BLL.Service;
using DAL.Interface;
using DAL.Operation;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace reporting_api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            // Add services to the container.
            builder.Services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                               .AllowAnyMethod()
                               .AllowAnyHeader();
                    });
            });

            builder.Services.Configure<RouteOptions>(options =>
            {
                options.LowercaseUrls = true;
            });

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            builder.Services.AddHttpClient("Order-api", client =>
            {
                client.BaseAddress = new Uri(Environment.GetEnvironmentVariable("OrderApiUrl") ?? configuration.GetConnectionString("OrderApiUrl")); // URL de la premi�re API externe
            });

            builder.Services.AddScoped<IReportingOrderServices, ReportingOrderServices>();

            builder.Services.AddScoped<IReportingOrderOperation, ReportingOrderOperation>();

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Your API", Version = "v1" });

                if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ENV")))
                {
                    c.DocumentFilter<SwaggerDocumentFilter>();
                }
            });

            var app = builder.Build();
            app.UseCors("AllowAll");

            if (Environment.GetEnvironmentVariable("ENV") != "PROD")
            {
                app.UseSwagger();
                app.UseSwaggerUI();
                Console.WriteLine("Environnement : " + Environment.GetEnvironmentVariable("ENV"));

            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }

    }
    public class SwaggerDocumentFilter : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            // Pr�fixe � ajouter aux chemins des appels HTTP
            string pathPrefix = "/reporting";

            var modifiedPaths = new Dictionary<string, OpenApiPathItem>();

            foreach (var pathItem in swaggerDoc.Paths)
            {
                var newPathItem = new OpenApiPathItem();

                foreach (var operation in pathItem.Value.Operations)
                {
                    newPathItem.AddOperation(operation.Key, operation.Value);
                }

                modifiedPaths[pathPrefix + pathItem.Key] = newPathItem;
            }

            // Ajouter les chemins modifi�s au document Swagger
            foreach (var modifiedPath in modifiedPaths)
            {
                swaggerDoc.Paths[modifiedPath.Key] = modifiedPath.Value;
            }

            // Supprimer les anciens chemins
            foreach (var pathItemKey in modifiedPaths.Keys)
            {
                swaggerDoc.Paths.Remove(pathItemKey.Substring(pathPrefix.Length));
            }
        }
    }


}