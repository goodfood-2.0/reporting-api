﻿using DAL.Entity;
using DAL.Interface;
using Newtonsoft.Json;

namespace DAL.Operation
{
    public class ReportingOrderOperation : IReportingOrderOperation
    {

        private readonly HttpClient orderApi;
        public ReportingOrderOperation(IHttpClientFactory clientFactory)
        {
            orderApi = clientFactory.CreateClient("Order-api");
        }


        public async Task<List<TopBestContent>> GetOrderBestContents(int NumlberOfBest, int idRestaurant, DateTime dateMin, DateTime dateMax)
        {
            var formattedDateMin = dateMin.ToString("yyyy-MM-ddTHH:mm:ssZ");
            var formattedDateMax = dateMax.ToString("yyyy-MM-ddTHH:mm:ssZ");

            var url = $"/OrderContents/bestcontents?NumberOfBest={NumlberOfBest}&IdRestaurant={idRestaurant}&dateMin={formattedDateMin}&dateMax={formattedDateMax}";
            var response = await orderApi.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                List<TopBestContent> objResponse = JsonConvert.DeserializeObject<List<TopBestContent>>(jsonResponse);
                return objResponse;
            }

            return null;
        }

        public async Task<CountOrders> CountOrders(int idRestaurant, DateTime dateMin, DateTime dateMax)
        {

            var formattedDateMin = dateMin.ToString("yyyy-MM-ddTHH:mm:ssZ");
            var formattedDateMax = dateMax.ToString("yyyy-MM-ddTHH:mm:ssZ");

            var url = $"/orders/restaurant/{idRestaurant}/count?dateMin={formattedDateMin}&dateMax={formattedDateMax}";
            var response = await orderApi.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                CountOrders objResponse = JsonConvert.DeserializeObject<CountOrders>(jsonResponse);
                return objResponse;
            }

            return null;
        }

        public async Task<TotalAmount> GetTotalAmount(int idRestaurant, DateTime dateMin, DateTime dateMax)
        {
            var formattedDateMin = dateMin.ToString("yyyy-MM-ddTHH:mm:ssZ");
            var formattedDateMax = dateMax.ToString("yyyy-MM-ddTHH:mm:ssZ");

            var url = $"/OrderContents/totalamount?idRestaurant={idRestaurant}&dateMin={formattedDateMin}&dateMax={formattedDateMax}";
            var response = await orderApi.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                TotalAmount objResponse = JsonConvert.DeserializeObject<TotalAmount>(jsonResponse);
                return objResponse;
            }

            return null;
        }


        public async Task<List<TopSoldContent>> GetOrderBestSold(int NumlberOfBest, int idRestaurant, DateTime dateMin, DateTime dateMax)
        {
            var formattedDateMin = dateMin.ToString("yyyy-MM-ddTHH:mm:ssZ");
            var formattedDateMax = dateMax.ToString("yyyy-MM-ddTHH:mm:ssZ");

            var url = $"OrderContents/bestsold?NumberOfBest={NumlberOfBest}&IdRestaurant={idRestaurant}&dateMin={formattedDateMin}&dateMax={formattedDateMax}";
            var response = await orderApi.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                List<TopSoldContent> objResponse = JsonConvert.DeserializeObject<List<TopSoldContent>>(jsonResponse);
                return objResponse;
            }

            return null;
        }
    }
}
