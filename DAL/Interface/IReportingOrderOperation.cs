﻿using DAL.Entity;

namespace DAL.Interface
{
    public interface IReportingOrderOperation
    {
        public Task<CountOrders> CountOrders(int idRestaurant, DateTime dateMin, DateTime dateMax);
        public Task<List<TopBestContent>> GetOrderBestContents(int NumlberOfBest, int idRestaurant, DateTime dateMin, DateTime dateMax);
        public Task<List<TopSoldContent>> GetOrderBestSold(int NumlberOfBest, int idRestaurant, DateTime dateMin, DateTime dateMax);
        public Task<TotalAmount> GetTotalAmount(int idRestaurant, DateTime dateMin, DateTime dateMax);
    }
}
