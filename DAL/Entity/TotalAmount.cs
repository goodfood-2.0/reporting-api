﻿namespace DAL.Entity
{
    public class TotalAmount
    {
        public TotalAmount()
        {
        }

        public double Total { get; set; }
        public DateTime DateMin { get; set; }
        public DateTime DateMax { get; set; }
    }
}
