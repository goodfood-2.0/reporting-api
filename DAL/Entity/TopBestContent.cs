﻿namespace DAL.Entity
{
    public class TopBestContent
    {
        public TopBestContent()
        {

        }

        public int IdContent { get; set; }
        public int SalesCount { get; set; }

        public DateTime DateMin { get; set; }
        public DateTime DateMax { get; set; }
    }
}
