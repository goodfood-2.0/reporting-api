﻿namespace DAL.Entity
{
    public class TopSoldContent
    {
        public TopSoldContent()
        {
        }

        public int IdContent { get; set; }
        public double TotalPrice { get; set; }
        public DateTime DateMin { get; set; }
        public DateTime DateMax { get; set; }
    }
}
